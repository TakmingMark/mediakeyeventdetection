package com.example.backgroundmediabutton

import android.content.ComponentName
import android.media.session.MediaController
import android.os.Build
import android.os.Bundle
import android.os.RemoteException
import android.support.v4.media.MediaBrowserCompat
import android.support.v4.media.session.MediaControllerCompat
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*


/**
 * reference
 * 1.https://developer.android.com/guide/topics/media-apps/mediabuttons#mediabuttons-and-active-mediasessions
 * 2.https://code.tutsplus.com/zh-hant/tutorials/background-audio-in-android-with-mediasessioncompat--cms-27030
 */
class MainActivity : AppCompatActivity() {
    private var mediaBrowserCompat: MediaBrowserCompat? = null
    private var mediaControllerCompat: MediaControllerCompat? = null

    private val mediaBrowserCompatConnectionCallback: MediaBrowserCompat.ConnectionCallback =
        object : MediaBrowserCompat.ConnectionCallback() {
            override fun onConnected() {
                super.onConnected()
                try {
                    mediaControllerCompat = MediaControllerCompat(
                        this@MainActivity,
                        mediaBrowserCompat!!.sessionToken
                    )

                    //setting system mediaController is from this app.
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        mediaController=mediaControllerCompat?.mediaController as MediaController
                    }
                } catch (e: RemoteException) {
                }
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mediaBrowserCompat = MediaBrowserCompat(
            this, ComponentName(this, MediaKeyEventService::class.java),
            mediaBrowserCompatConnectionCallback, intent.extras
        )

        mediaBrowserCompat?.connect()

        button.setOnClickListener {
            mediaControllerCompat?.transportControls?.play()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mediaBrowserCompat?.disconnect()
    }
}

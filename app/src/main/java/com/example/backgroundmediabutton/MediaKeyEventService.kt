package com.example.backgroundmediabutton

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.os.Build
import android.os.Bundle
import android.os.PowerManager
import android.support.v4.media.MediaBrowserCompat
import android.support.v4.media.session.MediaSessionCompat
import android.text.TextUtils
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.media.MediaBrowserServiceCompat
import androidx.media.session.MediaButtonReceiver


class MediaKeyEventService : MediaBrowserServiceCompat() {
    companion object {
        private const val MEDIA_SESSION_COMPAT_TAG = "media_session_compat_tag"
        private const val FOREGROUND_ID = 2
        private const val CHANNEL_ID = "media_key_event_service_id"
        private const val CHANNEL_NAME = "media_key_event_service_name"
        private const val NOTIFICATION_CONTENT_TITLE = "MediaKeyEvent-Service"
        private const val NOTIFICATION_CONTENT_TEXT = "Running"
    }

    private var mediaSessionCompat: MediaSessionCompat? = null

    private val mediaSessionCallback: MediaSessionCompat.Callback =
        object : MediaSessionCompat.Callback() {
            override fun onPlay() {
                super.onPlay()
                Log.d("Mark", mediaSessionCompat?.isActive.toString())
                mediaSessionCompat?.isActive = true
            }

            override fun onMediaButtonEvent(mediaButtonEvent: Intent): Boolean {
                Log.d("Mark", "onMediaButtonEvent2")
                return super.onMediaButtonEvent(mediaButtonEvent)
            }
        }


    override fun onCreate() {
        super.onCreate()
        initMediaSession()
        startForeground(FOREGROUND_ID, getNotification())
    }

    override fun onDestroy() {
        super.onDestroy()
        mediaSessionCompat?.release()
        NotificationManagerCompat.from(this).cancel(FOREGROUND_ID)
    }

    private fun initMediaSession() {
        val mediaButtonReceiver = ComponentName(
            applicationContext,
            MediaButtonReceiver::class.java
        )

        mediaSessionCompat = MediaSessionCompat(
            applicationContext,
            MEDIA_SESSION_COMPAT_TAG,
            mediaButtonReceiver,
            null
        )

        mediaSessionCompat?.setCallback(mediaSessionCallback)

        //Latest libs is already set flags, so not set flags again.
//        mediaSessionCompat?.setFlags(MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS or MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS)

        mediaSessionCompat?.isActive = true

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            val mediaButtonIntent = Intent(Intent.ACTION_MEDIA_BUTTON)
            mediaButtonIntent.setClass(this, MediaButtonReceiver::class.java)
            val pendingIntent =
                PendingIntent.getBroadcast(this, 0, mediaButtonIntent, 0)

            mediaSessionCompat?.setMediaButtonReceiver(pendingIntent)
        }

        if (sessionToken == null)
            sessionToken = mediaSessionCompat?.sessionToken
    }

    //Not important for general audio service, required for class
    override fun onGetRoot(
        clientPackageName: String,
        clientUid: Int,
        rootHints: Bundle?
    ): BrowserRoot? {
        return if (TextUtils.equals(clientPackageName, packageName)) {
            BrowserRoot(getString(R.string.app_name), null)
        } else null
    }

    //Not important for general audio service, required for class
    override fun onLoadChildren(
        parentId: String,
        result: Result<List<MediaBrowserCompat.MediaItem?>?>
    ) {
        result.sendResult(null)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            MediaButtonReceiver.handleIntent(mediaSessionCompat, intent)
        }
        return super.onStartCommand(intent, flags, startId)
    }

    private fun getNotification(): Notification {
        val builder = NotificationCompat
            .Builder(this, CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .setContentTitle(NOTIFICATION_CONTENT_TITLE)
            .setContentText(NOTIFICATION_CONTENT_TEXT)
            .setOngoing(true)
            .setShowWhen(false)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel =
                NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_LOW)
            val notificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }

        return builder.build()
    }
}